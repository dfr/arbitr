'use strict'
const fs = require('fs')
const process = require('process')
const err = require('debug')('err')
const _ = require('lodash')
const ccxt = require('ccxt')
const Promise = require('bluebird') 

const BitfinexWS = require("./exch/bitfinex.js")
const HitbtcWS = require("./exch/hitbtc.js")
const BitmexWS = require("./exch/bitmex.js")
const OkcoinWS = require("./exch/okcoin.js")
const ExmoWS = require("./exch/exmo.js")

const Engine0 = require('tingodb')()
const Engine = Promise.promisifyAll(Engine0)

const DbPath = __dirname + '/data'
if(!fs.existsSync(DbPath)) fs.mkdirSync(DbPath)
const Dbh = new Engine.Db(DbPath, {})

const DryRun = false
const PAIRS = ['BTC/USD']
//const PAIRS = ['XRP/USD','XMR/USD']
//const PAIRS = ['XRP/BTC','ZEC/BTC','LTC/BTC']

const MinSpread = 1
const MinFallback = 0.1 // from initial spread
const VolumeUSD = 10

const ApiKeys = {
    bitmex: {
        key: 'nEwxa1G1oj3oOhTb0Ihik7hM',
        secret: 'IYQay-5-bIyhdASPpgXVrLzZqjnljQknqZ53M1alighXld-N'
    }
}

const Precisions = {
    'BTC/USD': 3
}

let WrkPairs = {} // { gdax-kraken: { st: 'open', ini: { spread: 5 }, ... } }
let WrkOpen = {} // { gdax: 1, kraken: 1 }
let MinMax = {} // { gdax-kraken: { min: 0, max: 10.0 },  }

const timer = ms => new Promise( res => setTimeout(res, ms));
process.on('unhandledRejection', error => {
    console.log('unhandledRejection', error);
});

function diffToKey(d) {
    let arr = [d.long.ex, d.short.ex];
    arr.sort()
    arr.unshift(d.pair)
    return arr.join('-')
}

function ffmt(n, d = 2) {
    return new Intl.NumberFormat('ru', { maximumFractionDigits: d }).format(n)
}

function calcComparison(pair, tickers) {
    let diffs = {}
    let diffs1 = []
    for(const ti1 of tickers) {
        diffs[ti1.ex] = {}
        for(const ti2 of tickers) {
            if(ti1.ex === ti2.ex) continue
            const longPrice = getLimitPrice(ti1.asks)
            const shortPrice = getLimitPrice(ti2.bids)
            const spread = longPrice == 0 ? 0 : (shortPrice - longPrice) / longPrice * 100
            // longId = t1.ex
            //  + ' = ' + `(${ti2.bid} - ${ti1.ask}) / ${ti1.ask}`
            if(spread > 0) {
                diffs[ti1.ex][ti2.ex] = spread
                diffs1.push({ spread, pair, long: ti1, short: ti2 })
            }
        }
    }
    //return diffs
    diffs1.sort((a, b) => Math.abs(b.spread) - Math.abs(a.spread))
    return diffs1
}

async function analyzeDiffs(wsExs, markets, diffs) {
    for(const diff of diffs) {
        const key = diffToKey(diff)
        // upd minmax
        if(!MinMax[key]) MinMax[key] = { min: 1000, max: 0, maxDate: new Date() }
        if(diff.spread > MinSpread && MinMax[key].min > diff.spread) {
            MinMax[key].min = diff.spread
        }
        if(MinMax[key].max < diff.spread) {
            MinMax[key].max = diff.spread
            MinMax[key].maxDate = new Date()
        }
        else if(WrkPairs[key] && WrkPairs[key].st === 'waiting' && new Date() - MinMax[key].maxDate > 3600000) {
            MinMax[key].maxDate = new Date()
            MinMax[key].max =  diff.spread + (MinMax[key].max - diff.spread) / 2
        }
        // process spreads
        if(!WrkPairs[key]) {
            if(diff.spread > MinSpread) WrkPairs[key] = { ini: diff, cur: diff,  st: 'waiting', trail: 0.0 }
        }
        else {
            WrkPairs[key].cur = diff
            // process work pairs with big spread
            if(WrkPairs[key].st === 'waiting') {
                if(WrkOpen[diff.long.ex] || WrkOpen[diff.short.ex] || diff.spread < MinSpread) {
                    delete WrkPairs[key]
                    continue
                }
                //if(_.keys(WrkOpen).length > 0) continue // TODO
                //err(`waiting spread for ${key}: `, ffmt(MinMax[key].min), ' - ', ffmt(diff.spread), ' - ', ffmt(MinMax[key].max))
                //err('diversity:', ffmt(MinMax[key].max - MinMax[key].min), ' > ',  ffmt(MinMax[key].max * 0.2))
                const goodDiversity = MinMax[key].max - MinMax[key].min >  MinMax[key].max * 0.2
                //const spreadOk = MinMax[key].max - diff.spread <  MinMax[key].max * 0.2 && diff.spread < MinMax[key].max
                const halfFactor = 0.75
                const spreadOk = diff.spread > MinMax[key].min + (MinMax[key].max - MinMax[key].min) * halfFactor && diff.spread < MinMax[key].max
                if(goodDiversity && spreadOk) {
                    err('spreadOk:',ffmt(diff.spread), ' > ', ffmt(MinMax[key].min + (MinMax[key].max - MinMax[key].min) * halfFactor), " ...", MinMax[key].max)
                    err(`opening positions for ${key}`)
                    const opened = await openPositions(wsExs, markets, diff)
                    if(opened) {
                        WrkPairs[key].st = 'open'
                        WrkPairs[key].openDiff = diff
                        WrkPairs[key].openDiff.date = new Date()
                        WrkPairs[key].openPrices = opened
                        WrkOpen[diff.long.ex] = true
                        WrkOpen[diff.short.ex] = true
                        Dbh.collection("wrkpairs").updateAsync({ _id: key }, { _id: key, ...WrkPairs[key] }, { upsert: true })
                    }
                }
            }
            else if(WrkPairs[key].st === 'trailing') {
                const newTrail = diff.spread - WrkPairs[key].ini.spread
                if(WrkOpen[diff.long.ex] || WrkOpen[diff.short.ex]) {
                    delete WrkPairs[key]
                    continue
                }
                err("trailing ", key, " spread:", ffmt(diff.spread), " fallback:", ffmt(WrkPairs[key].trail - newTrail))
                if(newTrail >= WrkPairs[key].trail) {
                    //if(newTrail > WrkPairs[key].trail) err('new trail for ', key, ':', ffmt(WrkPairs[key].trail))
                    WrkPairs[key].trail = newTrail
                }
                else {
                    const fallback = WrkPairs[key].trail - newTrail
                    if(fallback > WrkPairs[key].ini.spread*MinFallback) {
                        if(diff.spread > MinSpread + MinSpread/4) {
                            err("fallback is > MIN = ", fallback)
                            const opened = await openPositions(wxExs, markets, diff)
                            if(opened) {
                                WrkPairs[key].st = 'open'
                                WrkPairs[key].trail = 0
                                WrkPairs[key].openDiff = diff
                                WrkPairs[key].openPrices = opened
                                WrkOpen[diff.long.ex] = true
                                WrkOpen[diff.short.ex] = true
                            }
                        }
                        else {
                            delete WrkPairs[key]
                        }
                    }
                }
            }
            else if(WrkPairs[key].st === 'open') {
                const newTrail = WrkPairs[key].openDiff.spread - diff.spread
                const fallback = WrkPairs[key].trail - newTrail
                err("opened ", key, " spread:", ffmt(diff.spread), " trail:", ffmt(newTrail), " fb:", ffmt(fallback), ' > ', ffmt((diff.spread - MinMax[key].min)*MinFallback))
                if(newTrail >= WrkPairs[key].trail) {
                    if(newTrail > WrkPairs[key].trail) err('new trail for ', key, ':', ffmt(WrkPairs[key].trail))
                    WrkPairs[key].trail = newTrail
                }
                if(newTrail > MinSpread/4 && fallback > (diff.spread - MinMax[key].min)*MinFallback) {
                    const closed = await closePositions(wsExs, diff, WrkPairs[key].openPrices);
                    if(closed) {
                        const l = closed.long - WrkPairs[key].openPrices.long[0]
                        const s = WrkPairs[key].openPrices.short[0] - closed.short
                        const fee = WrkPairs[key].openPrices.long[1]*0.0025 + WrkPairs[key].openPrices.short[1]*0.0025
                        const profit = l*WrkPairs[key].openPrices.long[1] + s*WrkPairs[key].openPrices.short[1] - fee
                        err("profit:", ffmt(profit))
                        err("profit usd:", ffmt(profit / WrkPairs[key].openPrices.long[0]))
                        Dbh.collection("history").insertAsync({
                            key: key,
                            profitUSD: ffmt(profit / WrkPairs[key].openPrices.long[0]),
                            iniSpred: ffmt(WrkPairs[key].openDiff.spread),
                            finSpred: ffmt(diff.spread),
                            spreadDiff: ffmt(WrkPairs[key].openDiff.spread - diff.spread),
                            openedDate: WrkPairs[key].openDiff.date,
                            date: new Date(),
                            durationHrs: ffmt((new Date() - WrkPairs[key].openDiff.date) / 3600000, 1)
                        })
                        Dbh.collection("wrkpairs").removeAsync({ _id: key })
                        delete WrkPairs[key];
                        delete WrkOpen[diff.long.ex]
                        delete WrkOpen[diff.short.ex]
                    }
                }
                
            }
        }
    }
}


function getLimitPrice(book) {
    const vol = 1 / book[0][0] * VolumeUSD
    let v = 0, p = 0
    for (let i = 0; i < book.length; i++) {
        const el = book[i]; // [price, vol]
        v += +el[1]
        p = el[0]
        if(v >= vol) break;
    }
    return p
}

async function openPositions(wxExs, markets, diff) {
    const shortPrice = getLimitPrice(diff.short.bids)
    const longPrice = getLimitPrice(diff.long.asks)
    const prec = Precisions[diff.pair]
    const shortVol = ffmt(1 / shortPrice * VolumeUSD, prec)
    const longVol = ffmt(1 / longPrice * VolumeUSD, prec)
    err("short sell: ", shortPrice, " vol:", (shortVol), " ex:", diff.short.ex)
    err("long buy: ", longPrice, " vol:", (longVol), " ex:", diff.long.ex)
    let extra = {}
    if(!DryRun) {
        extra.balShort = await wxExs[diff.short.ex].fetchBalance()
        extra.balLong = await wxExs[diff.long.ex].fetchBalance()
        extra.balSum = +extra.balShort + +extra.balLong
        err(`balance ${diff.short.ex}: `, extra.balShort)
        err(`balance ${diff.long.ex}: `, extra.balLong)
        err(`summary balance:`, extra.balSum)
        const so = wxExs[diff.short.ex].createMarketSellOrder(diff.pair, shortVol)
        const lo = wxExs[diff.long.ex].createMarketBuyOrder(diff.pair, longVol)
        try {
            const r = await Promise.all([so,lo])
            //shortId = r[0].id
            //longId  = r[1].id
        }
        catch (e) {
            err('!!! problem while opening orders ', e)
            await timer(10000);
            return
        }
    }
    return { long: [longPrice, longVol], short: [shortPrice, shortVol], ...extra }
}

async function closePositions(wxExs, diff, opened) {
    err("closing positions")
    const shortPrice = getLimitPrice(diff.short.bids)
    const longPrice = getLimitPrice(diff.long.asks)
    const shortVol = opened.short[1]
    const longVol = opened.long[1]
    err("close short sell: ", shortPrice, " (opened at: ", opened.short[0],") ex:", diff.short.ex)
    err("close long buy: ", longPrice, " (opened at: ", opened.long[0],") ex:", diff.long.ex)
    let extra = {}
    if(!DryRun) {
        extra.balShort = await wxExs[diff.short.ex].fetchBalance()
        extra.balLong = await wxExs[diff.long.ex].fetchBalance()
        extra.balSum = +extra.balShort + +extra.balLong
        err(`balance ${diff.short.ex}: ${extra.balShort}`)
        err(`balance ${diff.long.ex}: ${extra.balLong}`)
        err(`summary balance:`, extra.balSum)
        err(`balance change:`, extra.balSum - opened.balSum)
        const so = wxExs[diff.short.ex].createMarketBuyOrder(diff.pair, shortVol)
        const lo = wxExs[diff.long.ex].createMarketSellOrder(diff.pair, longVol)
        try {
            const r = await Promise.all([so,lo])
        }
        catch (e) {
            err('!!! problem while closing orders ', e)
            await timer(5000)
            return
        }
    }
    return { long: longPrice, short: shortPrice, ...extra }
}

// helper
function mongoFindAll(col, q = {}) {
    return new Promise((res, rej) => {
        Dbh.collection(col).find(q).toArray((err, docs) => {
            if(err) rej(err)
            else res(docs)
        })
    })
}

async function main () {
    MinMax = await Dbh.collection("minmax").findOneAsync({ _id: 1 })
    err('got out of minmax collection: ', MinMax)
    if(!MinMax) MinMax = {}
    const wrk = await mongoFindAll("wrkpairs")
    for(const w of wrk) {
        WrkPairs[w._id] = _.omit(w, ['_id'])
        WrkOpen[WrkPairs[w._id].openDiff.short.ex] = true
        WrkOpen[WrkPairs[w._id].openDiff.long.ex] = true
    }

    let wsExs = {
        //okcoinusd: new OkcoinWS(PAIRS),
        bitmex: new BitmexWS(PAIRS, ApiKeys.bitmex),
        //bitfinex2: new BitfinexWS(PAIRS),
        exmo: new ExmoWS(PAIRS),
        //hitbtc2: new HitbtcWS(PAIRS),
    }
    
    //let bitfinex     = new ccxt.bitfinex2 ()
    let bitmex     = new ccxt.bitmex ()
    //let hitbtc     = new ccxt.hitbtc2 ()
    let exmo     = new ccxt.exmo ()
    //let okcoin = new ccxt.okcoinusd ()
    
    const exs = [bitmex, exmo];
    //err(' ', exs.map(ti => ti.id))
    
    let markets
    try {
        markets = _.fromPairs(await Promise.all(exs.map(ex => ex.loadMarkets().then(m => [ ex.id, m ]) )))
    }
    catch (e) {
        console.error("error loading markets", e)
        process.exitCode = 1;
    }

    await timer(1400);

    //err(markets['bitmex'])
    //err(await wsExs.bitfinex2.fetchOrderBook('LTC/USD'))
    
    while(true) {
        let fetchTime = 0
        for(const pair of PAIRS) {
            //err(`fetching order books for ${pair}...`)
            const start = new Date();
            const exs1 = exs.filter(ex => (pair in markets[ex.id]))
            let tickers = []
            try {
                tickers = await Promise.all(exs1.map(ex => {
                    let p = wsExs[ex.id] ? wsExs[ex.id].fetchOrderBook(pair) : ex.fetchOrderBook(pair)
                    return p.then(ti => Object.assign(ti, { ex: ex.id }))
                }))
            }
            catch (e) {
                console.error('Exception fetching books: ', e)
            }
            let tickers1 = (tickers || []).filter(ti => ti.bids && ti.asks && ti.bids.length > 0 && ti.asks.length > 0)
            //err(`done fetching order books for ${pair}...`)
            fetchTime += new Date() - start;
            if(tickers1.length > 1) {
                const diffs = calcComparison(pair, tickers1)
                await analyzeDiffs(wsExs, markets, diffs)
            }
            //err(pair, ' ', tickers1.map(ti => ti.ex))
        }
        await timer(400 - fetchTime);
        Dbh.collection("minmax").updateAsync({ _id: 1 }, { _id: 1, ...MinMax }, { upsert: true })
    }
    
    //console.log (bitfinex.id,  await bitfinex.fetchTicker ('BTC/USD'))
}

main().catch((rsn) => console.log(rsn))

