const OkcoinWS = require("./exch/okcoin.js")
const HitWS = require("./exch/hitbtc.js")
const assert = require('assert')

process.on('unhandledRejection', error => {
  console.log('unhandledRejection', error);
});

//assert.equal(null, err);
const timer = ms => new Promise( res => setTimeout(res, ms));

const p = 'BTC/USD'

async function main() {
  const polo = new OkcoinWS([p])
  const hit = new HitWS([p])
  await timer(2000)
  console.log(polo.book(p))
  console.log(hit.book(p))
}


main()


