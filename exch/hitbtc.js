'use strict'

const { EventEmitter } = require('events')
const debug = require('debug')('hitbtc:ws')
const _ = require('lodash')
const err = require('debug')('err')
const crypto = require('crypto')
const WebSocket = require('ws')
const { toCCXTBook, BOOK_SIZE } = require('../helper.js')

const API_KEY = 'secret'
const API_SECRET = 'secret'


function passThrough (d) { return d }
/**
 * Handles communitaction with Bitfinex WebSocket API.
 * @param {string} APIKey
 * @param {string} APISecret
 * @param {object} Options
 * @event
 * @class
 */
class HitWS2 extends EventEmitter {
  constructor (apiKey, apiSecret, opts = {}) {
    super()
    this.apiKey = apiKey
    this.apiSecret = apiSecret
    this.websocketURI = opts.websocketURI || 'wss://api.hitbtc.com/api/2/ws'
    this.transformer = opts.transformer || passThrough
  }

  open () {
    this.ws = new WebSocket(this.websocketURI)
    this.ws.on('message', this.onMessage.bind(this))
    this.ws.on('open', this.onOpen.bind(this))
    this.ws.on('error', this.onError.bind(this))
    this.ws.on('close', this.onClose .bind(this))
  }

  onMessage (msg, flags) {
    try {
      msg = JSON.parse(msg)
    } catch (e) {
      console.error('[hitbtc ws2 error] received invalid json')
      console.error('[hitbtc ws2 error]', msg)
      console.trace()
      return
    }

    debug('Received message: %j', msg)
    debug('Emited message event')
    this.emit('message', msg, flags)

    if(msg.error) {
        this.emit('error', msg.error)
        debug('Emitting \'error\' %j', msg.error)
    }
    else if (msg.result) {
      if (msg.id.indexOf('subscribe') === 0) {
        debug('Subscription report received')
        this.emit('subscribed', 'ok')
      } else {
        debug('Emitting \'%s\' %j', msg.event, msg)
        this.emit(msg.event, msg)
      }
    } else if(msg.method) {
        if (msg.method && (msg.method === 'snapshotOrderbook' || msg.method === 'updateOrderbook' )) {
            this.emit('orderbook', msg.params.symbol, msg)
        }
    }
  }

   close () {
    this.ws.close()
  }

  onOpen () {
    this.channelMap = {} // Map channels IDs to events
    debug('Connection opening, emitting open')
    this.emit('open')
  }

  onError (error) {
    this.emit('error', error)
  }

  onClose () {
    this.emit('close')
  }

  send (msg) {
    debug('Sending %j', msg)
    this.ws.send(JSON.stringify(msg))
  }

  subscribeOrderBook (symbol = 'BTCUSD', precision = 'P0', length = '25') {
    this.send({
        "method": "subscribeOrderbook",
        "params": {
          "symbol": symbol
        },
        "id": "subscribe"
      })
  }

  unsubscribe (chanId) {
    this.send({
      event: 'unsubscribe',
      chanId
    })
  }

  submitOrder (order) {
    this.send(order)
  }

  cancelOrder (orderId) {
    this.send([0, 'oc', null, {
      id: orderId
    }])
  }

  config (flags) {
    this.send({
      flags,
      'event': 'conf'
    })
  }

  auth (calc = 0) {
    this.send({
        "method": "login",
        "params": {
          "algo": "BASIC",
          "pKey": this.apiKey,
          "sKey": this.apiSecret
        }
    })
  }

}

class Hitbtc {
    constructor(pairs) {
        this.books = {}
        this.pairs = pairs || []
        const ws = new HitWS2(API_KEY, API_SECRET)
        ws.open()
       
        ws.on('open', () => {
            for(const p of pairs) {
                debug('Subscribing to book for ' + p.replace(/[-_\/]/, ''))
                ws.subscribeOrderBook(p.replace(/[-_\/]/, ''))
            }
            // authenticate
            // ws.auth()
        })
        
        // { price: '14390.52', size: '0.10' }
        ws.on('orderbook', (pair, book) => {
            if(book.method === 'snapshotOrderbook') {
                if(!(pair in this.books)) this.books[pair] = { bids: [], asks: [] }
                for (let i = 0; i < BOOK_SIZE; i++) {
                    if(book.params.ask[i]) {
                      const r = book.params.ask[i]
                      this.books[pair].asks.push({ price: +r.price, size: +r.size })
                    }
                    if(book.params.bid[i]) {
                      const r = book.params.bid[i]
                      this.books[pair].bids.push({ price: +r.price, size: +r.size })
                    }
                }
            }
            else if (book.method === 'updateOrderbook') {
                for (let i = 0; i < book.params.ask.length; i++) {
                  const r = book.params.ask[i]
                  this.updateOrdBook('asks', pair, { price: +r.price, size: +r.size })
                }
                for (let i = 0; i < book.params.bid.length; i++) {
                  const r = book.params.bid[i]
                  this.updateOrdBook('bids', pair, { price: +r.price, size: +r.size })
                }
            }
        })
        
        ws.on('error', console.error)
        
        this.ws = ws
    }

    updateOrdBook(side, pair, r) {
        const idx = _.sortedIndexBy(this.books[pair][side], r, (v) => { return side === 'bids' ? -v.price : v.price })
        const replace = idx < this.books[pair][side].length && this.books[pair][side][idx].price === r.price ? 1 : 0;
        if(+r.size > 0) {
          this.books[pair][side].splice(idx, replace, r);
        }
        else {
          this.books[pair][side].splice(idx, replace);
        }
        this.books[pair][side].splice(BOOK_SIZE);
    }

    book(pair) {
        let p = pair.replace(/[-_\/]/, '')
        return this.books[p]
    }

    fetchOrderBook(pair) {
      const book = this.book(pair)
      return new Promise(res => res(toCCXTBook(book)))
    }
}


module.exports = Hitbtc
