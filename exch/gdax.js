'use strict'

const { EventEmitter } = require('events')
const debug = require('debug')('gdax:ws')
const err = require('debug')('err')
const _ = require('lodash')
const crypto = require('crypto')
const WebSocket = require('ws')
const { toCCXTBook, BOOK_SIZE } = require('../helper.js')

const API_KEY = 'secret'
const API_SECRET = 'secret'


function passThrough (d) { return d }
/**
* Handles communitaction with Bitfinex WebSocket API.
* @param {string} APIKey
* @param {string} APISecret
* @param {object} Options
* @event
* @class
*/
class GdaxWS2 extends EventEmitter {
  constructor (apiKey, apiSecret, opts = {}) {
    super()
    this.apiKey = apiKey
    this.apiSecret = apiSecret
    this.doAuth = opts.auth || false
    this.websocketURI = opts.websocketURI || 'wss://ws-feed.gdax.com'
    this.transformer = opts.transformer || passThrough
  }
  
  open () {
    this.ws = new WebSocket(this.websocketURI)
    this.ws.on('message', this.onMessage.bind(this))
    this.ws.on('open', this.onOpen.bind(this))
    this.ws.on('error', this.onError.bind(this))
    this.ws.on('close', this.onClose.bind(this))
  }
  
  onMessage (msg, flags) {
    try {
      msg = JSON.parse(msg)
    } catch (e) {
      console.error('[hitbtc ws2 error] received invalid json')
      console.error('[hitbtc ws2 error]', msg)
      console.trace()
      return
    }
    
    debug('Received message: %j', msg)
    debug('Emited message event')
    this.emit('message', msg, flags)
    
    if(msg.type === 'error') {
      this.emit('error', msg.message)
      debug('Emitting \'error\' %j', msg.message)
    }
    else if (msg.type === 'subscriptions') {
      debug('Subscription report received')
      this.emit('subscribed', 'ok')
    }
    else if (msg.type === 'snapshot' || msg.type === 'l2update') {
      this.emit('orderbook', msg.product_id, msg)
    }
  }
  
  close () {
    this.ws.close()
  }
  
  onOpen () {
    this.channelMap = {} // Map channels IDs to events
    debug('Connection opening, emitting open')
    this.emit('open')
  }
  
  onError (error) {
    this.emit('error', error)
  }
  
  onClose () {
    this.emit('close')
  }
  
  send (msg) {
    debug('Sending %j', msg)
    this.ws.send(JSON.stringify(msg))
  }
  
  subscribeOrderBook (symbol = 'BTCUSD', precision = 'P0', length = '25') {
    const sym1 = symbol.slice(0,3)
    const sym2 = symbol.slice(3)
    let data = {
      "type": "subscribe",
      "product_ids": [
        `${sym1}-${sym2}`
      ],
      "channels": [
        "level2"
      ]
    }
    if(this.doAuth && this.apiKey && this.apiSecret) {
      let timestamp = Date.now() / 1000
      let key = Buffer(this.apiSecret, 'base64');
      let hmac = crypto.createHmac('sha256', key);
      let what = timestamp + 'GET' + '/users/self/verify';
      let sig = hmac.update(what).digest('base64');
      
      dats.signature = sig
      data.key = this.apiKey
      data.passphrase = this.apiPass
      data.timestamp = timestamp
    }
    this.send(data)
  }
  
  unsubscribe (chanId) {
    this.send({
      type: 'unsubscribe',
      channels: ["level2"]
    })
  }
  
  submitOrder (order) {
    this.send(order)
  }
  
  cancelOrder (orderId) {
    this.send([0, 'oc', null, {
      id: orderId
    }])
  }
  
  config (flags) {
    this.send({
      flags,
      'event': 'conf'
    })
  }
  
  auth (calc = 0) {
    this.doAuth = true
  }
  
}

class Gdax {
  constructor(pairs) {
    this.books = {}
    this.pairs = pairs || []
    const ws = new GdaxWS2(API_KEY, API_SECRET)
    ws.open()
    
    ws.on('open', () => {
      for(const p of pairs) {
        debug('Subscribing to book for ' + p.replace(/[-_\/]/, ''))
        //ws.doAuth = true
        ws.subscribeOrderBook(p.replace(/[-_\/]/, ''))
      }
    })
    
    ws.on('orderbook', (pair1, book) => {
      let pair = pair1.replace('-', '')
      if(book.type === 'snapshot') {
        // "bids": [["1", "2"]], "asks": [[price, size]]
        if(!(pair in this.books)) this.books[pair] = { bids: [], asks: [] };
        for (let i = 0; i < BOOK_SIZE; i++) {
          if(book.asks[i]) this.books[pair].asks.push({ price: +book.asks[i][0], size: +book.asks[i][1] })
          if(book.bids[i]) this.books[pair].bids.push({ price: +book.bids[i][0], size: +book.bids[i][1] })
        }
      }
      else if (book.type === 'l2update') {
        // { "changes": [["buy", "1", "3"], ...] }
        for (let i = 0; i < book.changes.length; i++) {
          const [side1, price1, size] = book.changes[i];
          const price = price1.replace(/\.?0+$/, '')
          const side = side1 === 'sell' ? 'asks' : 'bids'
          this.updateOrdBook(side, pair, { price: +price, size: +size })
        }
      }
    })
    
    ws.on('error', console.error)
    
    this.ws = ws
  }
  
  updateOrdBook(side, pair, r) {
    const idx = _.sortedIndexBy(this.books[pair][side], r, (v) => { return side === 'bids' ? -v.price : v.price });
    const replace = idx < this.books[pair][side].length && this.books[pair][side][idx].price === r.price ? 1 : 0;
    if(+r.size > 0) {
      this.books[pair][side].splice(idx, replace, r);
    }
    else {
      this.books[pair][side].splice(idx, replace);
    }
    
    this.books[pair][side].splice(BOOK_SIZE);
  }
  
  book(pair) {
    let p = pair.replace(/[-_\/]/, '')
    return this.books[p]
  }

  fetchOrderBook(pair) {
    const book = this.book(pair)
    return new Promise(res => res(toCCXTBook(book)))
  }

}


module.exports = Gdax