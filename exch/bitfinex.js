
const _ = require('lodash');
const BFX = require('bitfinex-api-node')
const { toCCXTBook, BOOK_SIZE } = require('../helper.js')

const API_KEY = 'secret'
const API_SECRET = 'secret'

const opts = {
    version: 2,
    transform: true
}

class Bitfinex {
    constructor(pairs) {
        this.books = {}
        const bws = new BFX(API_KEY, API_SECRET, opts).ws

        this.pairs = pairs || []
        
        bws.on('auth', () => {
            // emitted after .auth()
            // needed for private api endpoints
            
            console.log('authenticated')
            // bws.submitOrder ...
        })
        
        bws.on('open', () => {
            for(const p of pairs) {
                bws.subscribeOrderBook(p.replace(/[-_\/]/, ''))
            }
            // bws.auth()
        })
        
        // { PRICE: 9887, COUNT: 0, AMOUNT: 1 }
        bws.on('orderbook', (pair, book) => {
            if(Array.isArray(book)) {
                if(!(pair in this.books)) this.books[pair] = { bids: [], asks: [] };
                for (let i = 0; i < book.length; i++) {
                    const r = book[i];
                    const side = r.AMOUNT >= 0 ? 'bids' : 'asks'
                    this.books[pair][side].push({ size: Math.abs(r.AMOUNT), price: r.PRICE })
                }
                //console.log(this.books[pair])
            }
            else {
                const side = book.AMOUNT >= 0 ? 'bids' : 'asks'
                const r = { size: Math.abs(book.AMOUNT), price: book.PRICE }
                const i = _.sortedIndexBy(this.books[pair][side], r, (v) => { return side === 'bids' ? -v.price : v.price });
                const replace = i < this.books[pair][side].length && this.books[pair][side][i].price === r.price ? 1 : 0;
                if(book.COUNT > 0) {
                    this.books[pair][side].splice(i, replace, r);
                }
                else {
                    this.books[pair][side].splice(i, replace);
                }
                this.books[pair][side].splice(BOOK_SIZE);
            }
        })
        
        bws.on('error', console.error)
        
        this.ws = bws
    }

    book(pair) {
        let p = 't' + pair.replace(/[-_\/]/, '')
        return this.books[p]
    }

    fetchOrderBook(pair) {
        const book = this.book(pair)
        return new Promise(res => res(toCCXTBook(book)))
    }
  
}

module.exports = Bitfinex


