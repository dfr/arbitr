'use strict'

const { EventEmitter } = require('events')
const debug = require('debug')('bitmex:ws')
const err = require('debug')('err')
const _ = require('lodash')
const crypto = require('crypto')
const WebSocket = require('ws')
const ccxt = require('ccxt')
const { toCCXTBook, BOOK_SIZE } = require('../helper.js')

const API_KEY = 'secret'
const API_SECRET = 'secret'


function passThrough (d) { return d }
/**
* Handles communitaction with Bitfinex WebSocket API.
* @param {string} APIKey
* @param {string} APISecret
* @param {object} Options
* @event
* @class
*/
class BitmexWS2 extends EventEmitter {
  constructor (apiKey, apiSecret, pairs = []) {
    super()
    this.apiKey = apiKey
    this.apiSecret = apiSecret
    const subscrStr = pairs.map(p => p.replace(/[-_\/]/, ''))
      .map(p => p.replace('BTC', 'XBT'))
      .map(p => `orderBook10:${p}`).join(',')
    this.websocketURI = 'wss://www.bitmex.com/realtime?subscribe=' + subscrStr
  }
  
  open () {
    this.ws = new WebSocket(this.websocketURI)
    this.ws.on('message', this.onMessage.bind(this))
    this.ws.on('open', this.onOpen.bind(this))
    this.ws.on('error', this.onError.bind(this))
    this.ws.on('close', this.onClose.bind(this))
  }
  
  onMessage (msg, flags) {
    try {
      msg = JSON.parse(msg)
    } catch (e) {
      console.error('[bitmex ws2 error] received invalid json')
      console.error('[bitmex ws2 error]', msg)
      console.trace()
      return
    }
    
    debug('Received message: %j', msg)
    debug('Emited message event')
    this.emit('message', msg, flags)
    
    if(msg.type === 'error') {
      this.emit('error', msg.message)
      debug('Emitting \'error\' %j', msg.message)
    }
    else if (msg.table === 'orderBook10') {
      this.emit('orderbook', msg)
    }
  }
  
  close () {
    this.ws.close()
  }
  
  onOpen () {
    this.channelMap = {} // Map channels IDs to events
    debug('Connection opening, emitting open')
    this.emit('open')
  }
  
  onError (error) {
    this.emit('error', error)
  }
  
  onClose () {
    this.emit('close')
  }
  
  send (msg) {
    debug('Sending %j', msg)
    this.ws.send(JSON.stringify(msg))
  }
  
  subscribeOrderBook (symbol = 'BTCUSD', precision = 'P0', length = '25') {
    const sym1 = symbol.slice(0,3)
    const sym2 = symbol.slice(3)
    let data = {
      "type": "subscribe",
      "product_ids": [
        `${sym1}-${sym2}`
      ],
      "channels": [
        "level2"
      ]
    }
    if(this.doAuth && this.apiKey && this.apiSecret) {
      let timestamp = Date.now() / 1000
      let key = Buffer(this.apiSecret, 'base64');
      let hmac = crypto.createHmac('sha256', key);
      let what = timestamp + 'GET' + '/users/self/verify';
      let sig = hmac.update(what).digest('base64');
      
      dats.signature = sig
      data.key = this.apiKey
      data.passphrase = this.apiPass
      data.timestamp = timestamp
    }
    this.send(data)
  }
  
  unsubscribe (chanId) {
    this.send({
      type: 'unsubscribe',
      channels: ["level2"]
    })
  }
  
  submitOrder (order) {
    this.send(order)
  }
  
  cancelOrder (orderId) {
    this.send([0, 'oc', null, {
      id: orderId
    }])
  }
  
  config (flags) {
    this.send({
      flags,
      'event': 'conf'
    })
  }
  
  auth (calc = 0) {
    this.doAuth = true
  }
  
}

class BitmexWrap {
  constructor(pairs, apiKey) {
    this.books = {}
    this.pairs = typeof pairs === 'object' ? pairs : []
    this.apiKey = apiKey
    this.ws = null
    this.ex = new ccxt.bitmex({ apiKey: apiKey.key, secret: apiKey.secret })

    if(pairs.length > 0) this.openSocket()
  }

  openSocket() {
    const ws = new BitmexWS2(this.apiKey.apiKey, this.apiKey.apiSecret, this.pairs)
    ws.open()
    
    ws.on('orderbook', (book) => {
      for (let i = 0; i < book.data.length; i++) {
        const r = book.data[i];
        const pair = r.symbol
        this.books[pair] = {
           bids: r.bids.map(([p, s]) => { return { price: +p, size: +s } }),
           asks: r.asks.map(([p, s]) => { return { price: +p, size: +s } })
        };
      }
    })
    
    ws.on('error', console.error)
    this.ws = ws
  }

  
  book(pair) {
    let p = pair.replace(/[-_\/]/, '').replace('BTC', 'XBT')
    return this.books[p]
  }

  fetchOrderBook(pair) {
    const book = this.book(pair)
    return new Promise(res => res(toCCXTBook(book)))
  }

  createMarketSellOrder(pair, vol) {
    return this.ex.createMarketSellOrder(pair, vol)
  }

  createMarketBuyOrder(pair, vol) {
    return this.ex.createMarketBuyOrder(pair, vol)
  }

  createLimitSellOrder(pair, vol, price, params) {
    return this.ex.createLimitSellOrder(pair, vol, price, params)
  }

  createLimitBuyOrder(pair, vol, price, params) {
    return this.ex.createLimitBuyOrder(pair, vol, price, params)
  }

  cancelOrder (id, symbol, params) {
    return this.ex.cancelOrder(id, symbol, params)
  }

  fetchBalance () {
    return this.ex.fetchBalance().then((b) => {
      return b.total.BTC
    })
  }
}


module.exports = BitmexWrap