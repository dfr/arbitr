'use strict'

const { EventEmitter } = require('events')
const debug = require('debug')('exmo:ws')
const err = require('debug')('err')
const _ = require('lodash')
const crypto = require('crypto')
const { promiseTimeout, BOOK_SIZE } = require('../helper.js')
const ccxt = require('ccxt')

const API_KEY = 'secret'
const API_SECRET = 'secret'

class ExmoWrap {
  constructor(pairs) {
    this.books = {}
    this.booksDates = {}
    this.pairs = pairs || []
    this.curPair = ''
    this.ex     = new ccxt.exmo ()
    this.fetchBookThr = _.throttle(this.fetchBook.bind(this), 400)
  }

  fetchBook() {
    const pair = this.curPair
    if(!pair) return
    // check timeout
    const now = new Date()
    if(this.booksDates[pair]) {
      if(now - this.booksDates[pair] > 30000) {
        err('exmo pair ', pair, ' didnt update for 30 sec!')
        this.books[pair] = null
        this.booksDates[pair] = null
      }
    }
    // fetch
    promiseTimeout(1000, this.ex.fetchOrderBook(pair)).then(res => {
      if(!this.booksDates[pair]) {
        err('exmo pair ', pair, ' is updated!')
      }
      this.books[pair] = res
      this.booksDates[pair] = now
    }).catch(e => {
      //err('kraken fetch error: ', e)
    })
  }
  
  fetchOrderBook(pair) {
    this.curPair = pair
    this.fetchBookThr()
    const book = this.books[pair]
    return new Promise(res => res(book || { asks: [], bids: [] }))
  }

  createMarketSellOrder(pair, vol) {
    return new Promise(res => res({ id: 1}))
  }

  createMarketBuyOrder(pair, vol) {
    return new Promise(res => res({ id: 1}))
  }

  createLimitSellOrder(pair, vol, price, params = {}) {
    return new Promise(res => res({}))
  }

  createLimitBuyOrder(pair, vol, price, params = {}) {
    return new Promise(res => res({}))
  }

  fetchBalance () {
    return new Promise(res => res(0))
  }

}


module.exports = ExmoWrap