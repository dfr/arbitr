const PoloniexWS = require("./exch/poloniex.js")
const assert = require('assert')

process.on('unhandledRejection', error => {
  console.log('unhandledRejection', error);
});

//assert.equal(null, err);
const timer = ms => new Promise( res => setTimeout(res, ms));

const p = 'BTC/LTC'

async function main() {
  const polo = new PoloniexWS([p])
  await timer(10000)
  console.log(polo.book(p))
}


main()


