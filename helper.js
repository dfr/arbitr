'use strict'

exports.isSnapshot = isSnapshot
exports.toCCXTBook = toCCXTBook
exports.promiseTimeout = promiseTimeout
exports.BOOK_SIZE = 25

function isSnapshot (msg) {
  if (!msg[0]) return false

  if (Array.isArray(msg[0])) return true

  return false
}

function toCCXTBook (book) {
  return {
    bids: book && book.bids ? book.bids.map(b => { return [ b.price, b.size ] }) : [],
    asks: book && book.asks ? book.asks.map(b => { return [ b.price, b.size ] }) : []
  }
}

function promiseTimeout(ms, promise){

  // Create a promise that rejects in <ms> milliseconds
  let timeout = new Promise((resolve, reject) => {
    let id = setTimeout(() => {
      clearTimeout(id);
      reject('Timed out in '+ ms + 'ms.')
    }, ms)
  })

  // Returns a race between our timeout and the passed in promise
  return Promise.race([
    promise,
    timeout
  ])
}